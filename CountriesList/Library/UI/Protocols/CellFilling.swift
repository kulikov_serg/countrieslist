
//
//  File.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

protocol CellFilling {
    
    associatedtype Value
    
    func fill(with model: Value)
}
