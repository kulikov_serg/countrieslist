//
//  FoundationKitFunctions.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

import Foundation

public func toString(_ cls: AnyClass) -> String {
    return String(describing: cls)
}
