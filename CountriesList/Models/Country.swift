//
//  Country.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

import Foundation

struct Country: Codable {
    let name: String
    let capital: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case capital = "capitalCity"
    }
    
}
