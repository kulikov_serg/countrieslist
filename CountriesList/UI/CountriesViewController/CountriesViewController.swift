//
//  CountriesViewController.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class CountriesViewController: UIViewController, ViewControllerRootView, UITableViewDelegate, UITableViewDataSource {
   
    typealias RootViewType = CountriesView
   
    //Private properties
   private let viewModel = CountriesViewModel()
    
    //MARK: - LifeCicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bindViewModel()
        self.prepareTableView()
        self.load()
    }

    //MARK: Private methods
    
    private func prepareTableView() {
        let tableView = self.rootView.tableView
        tableView?.register(cellClass: CountryCell.self)
        tableView?.rowHeight = UITableViewAutomaticDimension
    }
    
    private func bindViewModel() {
        self.viewModel.countries
            .producer
            .take(during: self.reactive.lifetime)
            .observe(on: QueueScheduler.main)
            .startWithValues {[weak self] _ in
                self?.rootView.tableView?.reloadData()
        }
    }
    
    private func load() {
        self.viewModel.load()
    }
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.countries.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: toString(CountryCell.self)) as? CountryCell
        let model = self.viewModel.countries.value[indexPath.row]
        cell?.fill(with: model)
        
        return cell ?? UITableViewCell()
    }
}

