//
//  CountryCell.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell, CellFilling {
    
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var capitalLabel: UILabel?
    
    //Overriden methods

    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.reset()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reset()
    }
    
    //MARK: CellFilling

    func fill(with model: Country) {
        self.nameLabel?.text = model.name
        self.capitalLabel?.text = model.capital
    }
    
    //MARK: - Private methods
    
    private func reset() {
        self.nameLabel?.text = ""
        self.capitalLabel?.text = ""
    }
    
}
