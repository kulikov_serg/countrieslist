//
//  JsonParsing.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 10.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

import Foundation

struct JsonParsing {
    
    func countries(json: Any) -> [Country]? {
        var countries: [Country]?
        if let array: [Any] = json as? Array,
            let countriesArr = array.last,
            let data = try? JSONSerialization.data(withJSONObject: countriesArr,
                                                   options: JSONSerialization.WritingOptions.prettyPrinted) {
            countries = try? JSONDecoder().decode([Country].self, from: data)
        }
        
        return countries
    }

}
