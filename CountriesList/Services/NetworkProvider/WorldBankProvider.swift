//
//  WorldBankProvider.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

import Foundation
import ReactiveSwift

struct WorldBankProvider {
    
    //Private properties
    private let networkManager = NetworkManager()
    private let worldBankAPI = WorldBankServerAPI()
    
    //MARK: Public Methods
    
    func getCountries() -> SignalProducer<[Country]?, NSError> {
        let url = self.worldBankAPI.v2.getCountries
        let parameters: [String: Any] = ["format": "json"]
        
        return self.networkManager.request(url: url, parameters: parameters)
            .map { JsonParsing().countries(json: $0)
        }
    }
    
}
