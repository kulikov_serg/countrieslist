//
//  WorldBankServerAPI.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

struct WorldBankServerConstants {
    
    static let worldServerUrl = "http://api.worldbank.org"
    
}

struct WorldBankServerAPI {
    
    let v2: V2 = V2()
    
    struct V2 {
        private let version = "v2"
        
        //MARK: - Countries
        
        var getCountries: String {
            return "\(WorldBankServerConstants.worldServerUrl)/\(version)/countries"
        }
    }
    
}
