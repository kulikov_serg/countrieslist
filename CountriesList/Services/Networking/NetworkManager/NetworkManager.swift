//
//  NetworkManager.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

import Alamofire
import ReactiveSwift

class NetworkManager {
    
    let sessionManager = Alamofire.SessionManager.default
    
    // MARK: - Public methods
    
    func request(method: Alamofire.HTTPMethod = .get,
                 url: URLConvertible,
                 parameters: [String: Any]? = nil,
                 headers: [String: String]? = nil) -> SignalProducer<Any, NSError>
    {
        let manager = self.sessionManager
        
        return SignalProducer({ (observer, compositeDisposable) in
            let request = manager.request(url,
                                          method: method,
                                          parameters: parameters,
                                          headers: headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case let .success(value):
                        observer.send(value: value)
                        observer.sendCompleted()
                    case let .failure(error):
                        observer.send(error: error as NSError)
                    }
            }
            compositeDisposable.observeEnded {
                request.cancel()
            }
        })
    }
    
}
