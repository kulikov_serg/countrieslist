//
//  CountriesViewModel.swift
//  CountriesList
//
//  Created by Sergey Kulykov on 09.09.2018.
//  Copyright © 2018 Sergey Kulykov. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol ViewModelProtocol {
    func load()
}

class CountriesViewModel: ViewModelProtocol {
    
    let countries = MutableProperty<[Country]>([])
    
    //Private properties
    private let worldBankProvider = WorldBankProvider()
    private let lifeTime = Lifetime.make()
    
    //MARK: Public methods
    
    func load() {
        self.worldBankProvider
            .getCountries()
            .producer
            .take(during: self.lifeTime.lifetime)
            .observe(on: QueueScheduler.main)
            .startWithResult { [weak self] result in
                switch result {
                case .success(let values):
                    if let values = values {
                        self?.countries.value = values
                    }
                case .failure(let error):
                    print(error)
                }
        }
    }
    
}
